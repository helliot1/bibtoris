#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 10:01:56 2020

@author: harrison
"""

import os 
from flask import Flask, flash, request, redirect, render_template, url_for, send_from_directory

from werkzeug.utils import secure_filename

import tempfile 

tempdir = tempfile.TemporaryDirectory()

UPLOAD_FOLDER = tempdir.name
ALLOWED_EXTENSIONS = {'bib'}


def create_app(test_config=None):
    #create and configure the app 
    app = Flask(__name__,instance_relative_config=True)
    app.config['UPLOAD_FOLDER']= UPLOAD_FOLDER
    app.config.from_mapping(
            SECRET_KEY='dev',
            DATABASE = os.path.join(app.instance_path, 'bibweb.sqlite'),
            )
    if test_config is None:
        #load the instance config, if it exists, when not testing 
        app.config.from_pyfile('config.py', silent=True)
        
    else:
        #load the test config passed in
        app.config.from_mapping(test_config)
    
    #ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass
    
    def allowed_file(filename):
        return '.' in filename and filename.rsplit('.',1)[1].lower() in ALLOWED_EXTENSIONS
    from . import webwriter
    def is_bib(filename):
        if filename.rsplit('.',1)[1].lower()!='bib':
            return(filename)
        if filename.rsplit('.',1)[1].lower()=='bib':
            new_filename = webwriter.open_file(filename, UPLOAD_FOLDER)
        return(new_filename)

    #a simple page that says hello
    @app.route('/',methods=['GET','POST'])
    def upload_file():
        if request.method == 'POST':
            #Check if the post request has the file part
            if 'file' not in request.files:
                flash('No file part')
                return redirect(request.url)
            file = request.files['file']
            #if a user does not select file, browser also
            #submit an empty part without filename 
            if file.filename=='':
                flash('No selected file')
                return redirect(request.url)
            
            if file and allowed_file(file.filename):
                
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))

                new_filename= is_bib(filename)
                print(new_filename)
                if(new_filename==''):
                    return redirect(url_for('uploaded_file',filename=filename))
                else:
                    return redirect(url_for('uploaded_file',filename=new_filename))

                return redirect(url_for('uploaded_file',filename=filename))
            
        return render_template("base.html")
    @app.route('/uploads/<filename>')
    def uploaded_file(filename):
        return send_from_directory(app.config['UPLOAD_FOLDER'],filename)
        
    
    
    @app.route("/image.jpg")
    def image():
        return render_template("image.jpg")
    
    @app.route('/test')
    def test_html():
        return render_template("test.html")
    
    
    
    from . import db
    from . import home
    
    db.init_app(app)
    app.register_blueprint(home.bp)
    
    return app

    