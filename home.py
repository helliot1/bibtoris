#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 10:28:20 2020

@author: harrison
"""

import functools

from flask import (
        Blueprint, flash, g, redirect, render_template, request, session, url_for)
from werkzeug.security import check_password_hash, generate_password_hash
from bibweb.db import get_db

bp = Blueprint('home',__name__,url_prefix='/home')


@bp.route ('/welcome',methods=('GET','POST'))
def welcome():
    if request.method == 'POST':
        input_file = request.form['input_file']
        output_file = request.form['output_file']
        db = get_db()
        error = None
        
        if not input_file:
            error = 'Input file is required'
        elif not output_file:
            error = 'Output file is required'
        if error is None:
            return redirect(url_for('home.thanks'))
        flash(error)
    return render_template('home/welcome.html')


@bp.route('/thanks',methods=('GET','POST'))
def thanks():
    return render_template('home/thanks.html')




