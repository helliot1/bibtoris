#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 16:18:48 2020

@author: harrison
"""

def read_authors(s):
    
    d = {}
    with open("dict.txt") as f:
        for line in f:
            (key, val) = line.split()
            d[(key)] = val
    #accent checker
    letter=''
    if('{' in s):
        z=s[s.index('{'):s.index('}')]
        if(len(z)==2):
            mark = s[s.index('{')-1]

        else:
            mark = s[s.index('{')+1:s.index('}')-1]
        letter = s[s.index('}')-1]
        if(s[s.index('}')+1]=='}'):
                mark = s[s.index('{')+1:s.index('}')+1]
                letter=''
        key = mark+letter

        if key in d.keys():
            letter=d[key]
                
                
        if(len(z)==2):        
            s = s[0:s.index('{')-2] + letter + s[s.index('}')+1:len(s)]
        else:
            s=s[0:s.index('{')] + letter + s[s.index('}')+1:len(s)]
            if('}' in s):
                s=s[0:s.index(letter)+1] + s[s.index(letter)+2:len(s)]

            
        
    if(' and' in s):
        if(',' in s and s.index(' and')>s.index(',')):
            last = s[0:s.index(',')] + ', '
            first = s[s.index(',')+2:s.index(' and')]
        
        else:
            first = s[0:s.index(' ')]
            last = s[s.index(' ')+1:s.index(' and')]+', '
            if('.' in last):
                while('.' in last):
                    longer = len(last)-len(last[last.index('.')+2:len(last)])
                    last=last[last.index('.')+2:len(last)]
                    first = s[0:len(first)+longer]
                
        name = last + first  
        print(name)
        
        
        l = s[s.index(' and')+5:len(s)]
        read_authors(l)
    else:
        if(',' in s):
            last = s[0:s.index(',')] + ', '
            first = s[s.index(',')+2:len(s)]
        elif(' ' not in s):
            first=s
            last=''
        else:
            first = s[0:s.index(' ')]
            last = s[s.index(' ')+1:len(s)]+', '
            if('.' in last):

                while('.' in last):
                    longer = len(last)-len(last[last.index('.')+2:len(last)])
                    last=last[last.index('.')+2:len(last)]
                    first = s[0:len(first)+longer]
                    

        name = last + first        
        print(name)
    
    return(d)
    

read_authors(s="R. Garnett and Carr{\'e}, Florence and Hartemink, Alfred E and Hempel, Jonathan and Huising, Jeroen and Lagacherie, Philippe and McBratney, Alex B and McKenzie, Neil J and de Lourdes Mendon{\c{c}}a-Santos, Maria and others")
