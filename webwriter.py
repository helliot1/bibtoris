#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 20 12:11:46 2020

@author: harrison
"""

import bibtexparser
from bibtexparser.bparser import BibTexParser
from bibtexparser.customization import homogenize_latex_encoding
import os





def write(bib_database, filename, directory):


    filepath = os.path.join(directory, filename)
    f = open(filepath, "a")
    f.truncate(0)


    
    keys_list = list(bib_database.entries_dict.keys())
    res = 0 
    
    
    for x in range (len(keys_list)):
        res = keys_list[x]
        
        correct_key = find_entry_key(bib_database.entries_dict[res]['ENTRYTYPE'])
        f.write("TY - " + correct_key + '\n')
        
       #Authors have to be processed separately 
       
        if('author' in bib_database.entries_dict[res]):
            s = bib_database.entries_dict[res]['author']
            read_authors(f, s, 'AU - ')

            
            
        if('editor' in bib_database.entries_dict[res]):
            s = bib_database.entries_dict[res]['editor']
            read_authors(f, s, 'ED - ')
            
        
        #This is a list of all the .bib key words, and a list of all the .ris key words.
        
        RISkeys = [['address','annote','booktitle','Email',
                    'chapter','crossref','doi','edition','howpublished',
                    'institution','journal','key','month','note','number','organization',
                   'publisher','school','series','title','type','volume','url','year'],
                    ['AD - ','N1 - ','T2 - ','C1 - ','C2 - ','C3 - ','DO - ','ET - '
                     ,'C4 - ','PP - ','T2 - ','U1 - ','U2 - ','N1 - ','IS - ',
                     'C5 - ','PB - ','OP - ','C6 - ','TI - ','M3 - ','VL - ', 'UR - ', 'PY - ']]
        
        
    #compares the written bibliography to the keys in list 1; if it matches, its replaced by
    #the key in list two
    
        y=0
        while y in range(len(RISkeys[0])):
            if(RISkeys[0][y] in bib_database.entries_dict[res]):
                s = str(bib_database.entries_dict[res][RISkeys[0][y]])
                s = s.replace('{','')
                s = s.replace('}','')
                f.write(RISkeys[1][y] + s + '\n')
            y+=1
            
    #Pages have to be calculated separately
        if('pages' in bib_database.entries_dict[res]):
            SP = bib_database.entries_dict[res]['pages']
            if('-' in SP):
                LP = SP[SP.index('-')+2:]  
                SP = SP[0:SP.index('-')]  
            else:
                LP = SP
            f.write("SP - " + SP + '\n')
            f.write("EP - " + LP + '\n')
       
    #Adding the ER - at the end is necessary to process multiple bibliographies
        f.write("ER - \n")   
                
    
def find_entry_key(key):
    
        #List of all the different entry types a .bib and a .ris file can be
        entry_type_keys = [['article','book','booklet','conference','inbook','incollection',
                                'inproceedings','manual','masterthesis','misc','phdthesis',
                                'proceedings','techreport','unpublished'],['JOUR','BOOK','','CONF','','','CONF','PAMP',
                                            'THES', '', 'THES','BOOK','RPRT','UNPB']] 
        #Looks for matching key between the two
        if (key in entry_type_keys[0]):
            for x in range(len(entry_type_keys[1])):
                if(entry_type_keys[0][x]==key):
                    return entry_type_keys[1][x]
        
        
def read_authors(f, s, k):
    accent_dict = {}
    with open("/Users/harrison/project2/bibweb/dict.txt") as q:
        for line in q:
            (key, val) = line.split()
            accent_dict[(key)] = val
    #accent checker
    letter=''
    if('{' in s):
        z=s[s.index('{'):s.index('}')]
        if(len(z)==2):
            mark = s[s.index('{')-1]

        else:
            mark = s[s.index('{')+2:s.index('}')-1]
        letter = s[s.index('}')-1]
        if(s[s.index('}')+1]=='}'):
                mark = s[s.index('{')+1:s.index('}')+1]
                letter=''
        key = mark+letter
        
        if key in accent_dict.keys():
            letter=accent_dict[key]
                
                
        if(len(z)==2):        
            s = s[0:s.index('{')-2] + letter + s[s.index('}')+1:len(s)]
        else:
            s=s[0:s.index('{')] + letter + s[s.index('}')+1:len(s)]
            if('}' in s):
                s=s[0:s.index(letter)+1] + s[s.index(letter)+2:len(s)]

            
    
    if(' and' in s):
        if(',' in s and s.index(' and')>s.index(',')):
            last = s[0:s.index(',')] + ', '
            first = s[s.index(',')+2:s.index(' and')]
        
        else:
            first = s[0:s.index(' ')]
            last = s[s.index(' ')+1:s.index(' and')]+', '
            if('.' in last):
                while('.' in last):
                    longer = len(last)-len(last[last.index('.')+2:len(last)])
                    last=last[last.index('.')+2:len(last)]
                    first = s[0:len(first)+longer]
        
                
        name = last + first
        f.write(k + name + '\n')
        
        
        l = s[s.index(' and')+5:len(s)]
        read_authors(f, l, k)
    else:
        if(',' in s):
            last = s[0:s.index(',')] + ', '
            first = s[s.index(',')+2:len(s)]
        elif(' ' not in s):
            first=s
            last=''
        else:
            first = s[0:s.index(' ')]
            last = s[s.index(' ')+1:len(s)]+', '
            if('.' in last):

                while('.' in last):
                    longer = len(last)-len(last[last.index('.')+2:len(last)])
                    last=last[last.index('.')+2:len(last)]
                    first = s[0:len(first)+longer]
                    
        name = last + first  
        f.write(k + name + '\n')
    print(name)

    return(s)
    
    
#running the code down here

def open_file(filename, directory):
    open_file = directory + '/' + filename
    
    filename = filename[0:filename.index('.')]+'.ris'
    with open(open_file, 'r') as bibtex_file: #cv_nsf.bib
        bib_database = bibtexparser.bparser.BibTexParser(common_strings=True).parse_file(bibtex_file)
        #print(bib_database.entries)
        write(bib_database, filename, directory)
    return(filename)
    
